FROM golang
ADD . /app
WORKDIR /app
ENV GOOS=linux
ENV CGO_ENABLED=0
RUN go build -o app cmd/web/*
ENTRYPOINT /app/app

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /app/
COPY --from=0 /app/app .
COPY --from=0 /app/ui ./ui
EXPOSE 4000
CMD ["./app", "-conn='host=host.docker.internal port=5435 user=postgres password=root dbname=snippetbox sslmode=disable'"]